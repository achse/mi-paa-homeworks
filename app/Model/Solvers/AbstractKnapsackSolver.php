<?php

namespace Model\Solvers;

use Model\Instance;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\Output;
use Tracy\Debugger;

abstract class AbstractKnapsackSolver implements IKnapsackSolver {

    /** @var  int */
    protected $bestSolutionCombination;

    /** @var  int */
    protected $bestSolutionPrice;

    /** @var  int */
    protected $bestSolutionWeight;

    /** @var  Output */
    protected $output;

    /** @var array  */
    protected $settings = [
        'time' => false,
        'progressBar' => false,
        'verbose' => false,
    ];

    /** @var  ProgressBar */
    protected $progressBar;

    /** @var  int */
    protected $maxCombination;

    /** @var  double */
    protected $onePercentStep;

    /** @var  int */
    protected $barCounter;

    /** @var double  */
    protected $lastRunTime = 0;

    function __construct($output = null, $settings = [])
    {
        $this->output = $output;
        foreach ($settings as $key => $setting) {
            $this->settings[$key] = $setting;
        }
    }

    protected function reset()
    {
        $this->bestSolutionCombination = null;
        $this->bestSolutionPrice = null;
        $this->bestSolutionWeight = null;
        $this->barCounter = 0;
        $this->onePercentStep = 1;
    }

    /**
     * @param Instance $instance
     * @return KnapsackSolution
     * @throws NotSupportedException
     */
    public function solve(Instance $instance)
    {
        if ($instance->getSize() > 32 && PHP_INT_MAX < 9223372036854775807) {
            throw new NotSupportedException("For more then 32 items, you need to run in 64bit.");
        }

        if ($instance->getSize() > 64) {
            throw new NotSupportedException("This solver is not able to solve more then 64 values.");
        }

        $this->reset();

        $this->preRun($instance);
        $this->solveImplementation($instance);
        $this->postRun($instance);

        $solution = new KnapsackSolution(
            $instance,
            $this->bestSolutionPrice,
            $this->bestSolutionCombination,
            $this->lastRunTime
        );

        return $this->modifySolution($solution);
    }

    /* ************* HOOKS ************************ */

    protected  function preTest(Instance $instance, $combination)
    {
    }

    protected function postTest(Instance $instance, $combination)
    {
        if ($this->settings['progressBar']) {
            if ($this->barCounter >= $this->onePercentStep) {
                $this->progressBar->advance();
                $this->barCounter = 0;
            }
            $this->barCounter++;
        }
    }

    protected  function preRun(Instance $instance)
    {
        if ($this->settings['verbose']) {
            $this->output->writeln("\nStarting: {$instance->getId()}");
        }

        if ($this->settings['progressBar']) {
            $this->progressBar = new ProgressBar($this->output, 100);
            $this->progressBar->start();
        }

        if ($this->settings['time']) {
            Debugger::timer("Instance-{$instance->getId()}");
        }
    }

    protected function postRun(Instance $instance)
    {
        if ($this->settings['progressBar']) {
            $this->progressBar->finish();
            $this->output->writeln('');
        }

        if ($this->settings['time']) {
            $this->lastRunTime = Debugger::timer("Instance-{$instance->getId()}");
        }
    }

    abstract protected function solveImplementation(Instance $instance);

    /**
     * @param KnapsackSolution $solution
     * @return mixed
     *
     * This method can (and should) be overwritten to set some additional parameters to the solution
     * (for exammple expectedError, etc..)
     */
    protected function modifySolution(KnapsackSolution $solution)
    {
        return $solution;
    }
}