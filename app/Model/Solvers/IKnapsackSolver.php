<?php

namespace Model\Solvers;

use Model\Instance;

interface IKnapsackSolver
{

    /**
     * @param Instance $instance
     * @return KnapsackSolution
     */
    public function solve(Instance $instance);
}