<?php

namespace Model\Solvers\Hw01;

use Model\Instance;
use Model\Solvers\AbstractKnapsackSolver;

class KnapsackOptimalSolver extends AbstractKnapsackSolver
{

    protected function solveImplementation(Instance $instance)
    {
        $this->maxCombination = pow(2, $instance->getSize());
        $this->onePercentStep = $this->maxCombination / 100;

        for ($combination = 0; $combination < $this->maxCombination; $combination++) {
            $this->preTest($instance, $combination);
            $this->test($instance, $combination);
            $this->postTest($instance, $combination);
        }
    }

    protected function test(Instance $instance, $combination)
    {
        $sumPrice = 0;
        $sumWeight = 0;
        $mask = 1;
        $index = 0;
        $data = $instance->getData();

        do {

            if (($combination & $mask) != 0) {
                $sumWeight += $data[$index][0];
                $sumPrice += $data[$index][1];
            }

            if ($sumWeight > $instance->getCapacity()) {
                return;
            }

            $mask <<= 1;
            $index++;

        } while ($mask < (1 << $instance->getSize()));

        if (
            ($sumPrice > $this->bestSolutionPrice)
            OR
            ($sumPrice == $this->bestSolutionPrice && $sumWeight > $this->bestSolutionWeight) // We WANT to add ZERO Priced items
        ) {
            $this->bestSolutionCombination = $combination;
            $this->bestSolutionPrice = $sumPrice;
            $this->bestSolutionWeight = $sumWeight;
        }
    }


}