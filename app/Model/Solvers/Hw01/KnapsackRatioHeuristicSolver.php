<?php

namespace Model\Solvers\Hw01;

use Model\Instance;
use Model\Solvers\AbstractKnapsackSolver;
use Model\Solvers\KnapsackSolution;
use Nette\NotSupportedException;

class KnapsackRatioHeuristicSolver extends AbstractKnapsackSolver
{

    /** @var  array */
    protected $ratioTable;

    /**
     * @param Instance $instance
     * @throws NotSupportedException
     * @return KnapsackSolution
     */
    protected function solveImplementation(Instance $instance)
    {
        $data = $instance->getData();

        foreach ($data as $index => $row) {
            $this->ratioTable[] = [$row[1] / $row[0], $index];
        }

        // wo do not need to sort it, we can select maximal each round and get (n^2), with soring we would have (n^2)*log(n)

        $sumWeight = 0;
        $sumPrice = 0;
        $combination = 0;
        while (true) {

            if (count ($this->ratioTable) == 0) {
                break;
            }

            $index = $this->getNextMaxIndex();

            $sumWeight += $data[$index][0];
            $sumPrice += $data[$index][1];
            $combination |= (1 << $index);

            if ($sumWeight > $instance->getCapacity()) {
                break;
            }

            if ($sumPrice > $this->bestSolutionPrice) {
                $this->bestSolutionWeight = $sumWeight;
                $this->bestSolutionPrice = $sumPrice;
                $this->bestSolutionCombination = $combination;
            }
        }
    }

    protected function getNextMaxIndex() {
        $maxIndex = null;
        $maxRatio = 0;
        $indexToDelete = null;
        foreach ($this->ratioTable as $i => $row) {
            if ($row[0] >= $maxRatio) {
                $maxIndex = $row[1];
                $maxRatio = $row[0];
                $indexToDelete = $i;
            }
        }
        unset ($this->ratioTable[$indexToDelete]);
        return $maxIndex;
    }

    protected function reset()
    {
        parent::reset();
        $this->ratioTable = [];
    }

}