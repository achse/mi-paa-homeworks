<?php

namespace Model\Solvers\Hw02;

use Model\Instance;
use Model\Solvers\AbstractKnapsackSolver;
use Model\Utils\Calculator;
use Model\Utils\Printer;

class KnapsackDynamicSolver extends AbstractKnapsackSolver
{
    protected $solutionTable = [];

    public function solveImplementation(Instance $instance)
    {
        $data = $instance->getData();

        list($maxSumWeight, $maxSumPrice) = Calculator::calculateSumWeightAndPrice($data);

        $this->solutionTable[0][0] = 0;
        // 0 colm is INF
        for ($i = 1; $i <= $maxSumPrice; $i++) {
            $this->solutionTable[$i][0] = PHP_INT_MAX;
        }

        for ($c = 0; $c <= $maxSumPrice; $c++) {
            for ($i = 1; $i <= $instance->getSize(); $i++) {
                $this->getWight($data, $i, $c);
            }
        }

//        Printer::print2DArray($this->solutionTable);
//        die();

        $this->reconstructBestSolution($instance, $data, $maxSumPrice);

    }

    protected function getWight($data, $i, $c)
    {
        if ($data[$i - 1][1] <= $c) {
            $this->solutionTable[$c][$i] = min(
                $this->solutionTable[$c][$i - 1],
                ($this->getValue($c, $i - 1, $data) + $data[$i - 1][0])
            );

        } else {
            $this->solutionTable[$c][$i] = $this->solutionTable[$c][$i - 1];
        }
    }

    protected function getValue($c, $i, $data)
    {
        $cc = $c - $data[$i][1];
        if ($cc < 0) {
            return PHP_INT_MAX;
        } else {
            return $this->solutionTable[$cc][$i];
        }
    }

    protected function reset()
    {
        parent::reset();
        $this->solutionTable = [];
    }

    protected function reconstructBestSolution(Instance $instance, $data, $maxSumPrice)
    {
        $findWeight = $instance->getCapacity();
        $this->bestSolutionCombination = 0;

        for ($i = $instance->getSize(); $i > 0; $i--) {
            for ($c = $maxSumPrice; $c > 0; $c--) {

                if ($this->solutionTable[$c][$i] <= $findWeight) {

                    if ($i == $instance->getSize()) {
                        $this->bestSolutionPrice = $c;
                    }

                    if ($this->solutionTable[$c][$i] == $this->solutionTable[$c][$i - 1]) {
                        // -- none solution not here
                        $findWeight = $this->solutionTable[$c][$i];
                        break;
                    } else {
                        $this->bestSolutionCombination = ($this->bestSolutionCombination | (1 << ($i - 1)));
                        $findWeight = $this->solutionTable[$c][$i] - $data[$i - 1][0];
                        break;
                    }
                }
            }

            if ($findWeight == 0) {
                break;
            }
        }
    }
}