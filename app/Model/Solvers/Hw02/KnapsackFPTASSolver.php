<?php

namespace Model\Solvers\Hw02;

use Model\Instance;
use Model\Solvers\AbstractKnapsackSolver;
use Model\Solvers\KnapsackSolution;
use Model\Utils\Calculator;
use Nette\NotSupportedException;

class KnapsackFPTASSolver extends KnapsackDynamicSolver
{
    protected $relativeError;

    /** @var  KnapsackDynamicSolver */
    protected $dynamicSolver;

    function __construct($output = null, $settings = [])
    {
        parent::__construct($output, $settings);
        $this->dynamicSolver = new KnapsackDynamicSolver();
    }

    public function solve(Instance $instance)
    {
        $shift = $this->settings['shift'];

        $data = $instance->getData();
        list($maxSumWeight, $maxSumPrice) = Calculator::calculateSumWeightAndPrice($data);
        $this->relativeError = ($instance->getSize() * pow(2, $shift)) / $maxSumPrice;

        $shiftedData = [];
        foreach ($data as $k => $item) {
            $item[1] >>= $shift;
            $shiftedData[$k]= $item;
        }

        $shiftedInstance = clone $instance;
        $shiftedInstance->setData($shiftedData);

        $solution = parent::solve($shiftedInstance);

        // Recalculate price for real data (not shifted)
        $newPriceValue = 0;
        $combination = $solution->getCombination();
        $index = 0;
        while ($combination !== 0) {
            if ($combination & 1) {
                $newPriceValue += $data[$index][1];
            }
            $combination >>= 1;
            $index++;
        }
        $solution->setPriceValue($newPriceValue);

        return $solution;
    }


    protected function modifySolution(KnapsackSolution $solution)
    {
        $solution->setExpectedRelativeError($this->relativeError);
        return $solution;
    }

}