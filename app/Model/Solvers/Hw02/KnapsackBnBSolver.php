<?php

namespace Model\Solvers\Hw02;

use Model\Instance;
use Model\Solvers\AbstractKnapsackSolver;
use Model\Utils\Calculator;

class KnapsackBnBSolver extends AbstractKnapsackSolver
{
    /** @var  array */
    protected $bestState;


    protected function solveImplementation(Instance $instance)
    {
        $this->recursiveSolve($instance, $instance->getData());
    }

    protected function recursiveSolve(Instance $instance, array $haystack, array $state = [], $combination = 0, $deep = 0)
    {
        // B&B - WIGHT! optimization test if making this branch makes sense
        list($currentWeight, $currentPrice) = Calculator::calculateSumWeightAndPrice($state);
        if ($currentWeight > $instance->getCapacity()) {
            return;
        }

        // B&B - PRICE! optimization test if making this branch makes sense
        if ($this->bestSolutionPrice > $this->calculatePotentiallyPrice($state, $haystack)) {
            return;
        }

        // Test if this state is better then the one before
        if ($this->bestSolutionPrice === null || $this->bestSolutionPrice <= $currentPrice) {
            $this->bestSolutionPrice = $currentPrice;
            $this->bestSolutionWeight = $currentWeight;
            $this->bestState = $state;
            $this->bestSolutionCombination = $combination;
        }

        // END-ing condition
        if (!count($haystack)) {
            return;
        }

        // Spawn new states
        $item = array_pop($haystack);
        $deep++;
        //      Item NOT added
        $this->recursiveSolve($instance, $haystack, $state, $combination, $deep);
        //      Item added!
        $state[] = $item;
        $newCombination = ($combination | (1 << ($deep - 1)));
        $this->recursiveSolve($instance, $haystack, $state, $newCombination, $deep);
    }

    protected function calculatePotentiallyPrice($state, $haystack)
    {
        $sum = 0;
        foreach (array_merge($state, $haystack) as $item) {
            $sum += $item[1];
        }
        return $sum;
    }

}