<?php

namespace Model\Solvers;


use Model\Instance;
use Nette\Object;

class KnapsackSolution extends Object
{
    const REVERSED_COMBINATION_FORMAT = true;

    const NOT_REVERSED_COMBINATION_FORMAT = false;

    /** @var  Instance */
    protected $instance;

    /** @var  int */
    protected $priceValue;

    /** @var  int */
    protected $combination;

    /** @var  double */
    protected $timeToGet;

    /** @var  boolean */
    protected $flagReversed;

    protected $expectedRelativeError;

    function __construct($instance, $priceValue, $combination, $timeToGet, $flagReversed = true)
    {
        $this->combination = $combination;
        $this->instance = $instance;
        $this->priceValue = $priceValue;
        $this->timeToGet = $timeToGet;
        $this->flagReversed = $flagReversed;
    }


    public function toString($pre = '', $post = '') {
        $binaryString = str_pad(decbin($this->combination), $this->instance->getSize(), "0", STR_PAD_LEFT);

        $array = str_split($binaryString);
        if ($this->flagReversed) {
            $array = array_reverse($array);
        }

        $formattedBinaryString = join(' ', $array);

        if ($this->expectedRelativeError !== null) {
            $post = "   " . round($this->getExpectedRelativeError(), 3) . " {$post}";
        }

        $dbg = "" ;/* .
            "[Size: " . $this->instance->getSize() . "]" .
            "[Capacity: " . $this->instance->getCapacity() . "]" .
            " | "; //*/

        return "$dbg{$pre}{$this->instance->getId()} {$this->instance->getSize()} {$this->priceValue}  {$formattedBinaryString}{$post}";
    }

    public function getAccuracy($referenceSolution)
    {
        return round($this->priceValue / $referenceSolution, 3);
    }

    public function getAccuracyInPercent($referenceSolution) {
        return $this->getAccuracy($referenceSolution) * 100;
    }

    public function getTimeToGet()
    {
        return $this->timeToGet;
    }

    public function getPriceValue()
    {
        return $this->priceValue;
    }

    public function setExpectedRelativeError($expectedRelativeError)
    {
        $this->expectedRelativeError = $expectedRelativeError;
    }

    public function getExpectedRelativeError()
    {
        return $this->expectedRelativeError;
    }

    public function getFormatedExpectedRelativeError()
    {
        return round($this->expectedRelativeError, 2) . "%";
    }

    public function setPriceValue($priceValue)
    {
        $this->priceValue = $priceValue;
    }

    protected function getExpectedRelativeAccuracy()
    {
        return 1 - $this->expectedRelativeError;
    }

    public function getCombination()
    {
        return $this->combination;
    }

    public function getInstance()
    {
        return $this->instance;
    }

}