<?php

namespace Model\Solvers\Hw04\Genetic;


use Model\Instance;
use Model\Solvers\KnapsackSolution;
use Nette\Object;

class Individual extends Object {

    /** @var  KnapsackSolution */
    protected $solution;

    /** @var  int */
    protected $weightValue;

    protected $age = 0;

    function __construct($solution, $weightValue)
    {
        $this->solution = $solution;
        $this->weightValue = $weightValue;
    }

    public function getSolution()
    {
        return $this->solution;
    }

    public function getQuality(Individual $bestEverSeen)
    {
        return $this->getPriceValue() / $bestEverSeen->getPriceValue();
    }

    public function getWeightValue() {
        return $this->weightValue;
    }

    public function getPriceValue() {
        return $this->getSolution()->getPriceValue();
    }

    public function isValid()
    {
        return (int) ($this->getWeightValue() <= $this->getSolution()->getInstance()->getCapacity());
    }

    public function getPartForReproduction($combination)
    {
        $this->age++;
        return $this->solution->getCombination() & $combination;
    }

    public function getAge()
    {
        return $this->age;
    }

} 