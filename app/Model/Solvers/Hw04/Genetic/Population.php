<?php

namespace Model\Solvers\Hw04\Genetic;


use Model\Instance;
use Model\NoSolutionFoundException;
use Model\Solvers\KnapsackSolution;
use Nette\Object;

class Population extends Object
{

    /** @var  Array */
    protected $individuals = [];

    /** @var \Model\Instance */
    protected $instance;

    protected $generation = 0;

    /** @var Individual */
    protected $bestIndividualEverSeen = null;

    protected $sizeOfBasicPopulation;

    protected $growth;

    function __construct(Instance $instance, $sizeOfBasicPopulation = 2, $growth = 2)
    {
        $this->sizeOfBasicPopulation = $sizeOfBasicPopulation;
        $this->instance = $instance;
        $this->growth = $growth;
        $this->generateFirstGeneration();
    }

    protected function getRandomNewIndividual()
    {
        $combination = $this->generateLongIntCombination();
        return $this->createNewIndividual($combination);
    }

    protected function createNewIndividual($combination)
    {
        $data = $this->instance->getData();
        $i = 0;
        $priceValue = 0;
        $weightValue = 0;
        while ($i <= $this->instance->getSize()) {
            if ($combination & (1 << $i)) {
                $weightValue += $data[$i][0];
                $priceValue += $data[$i][1];
            }

            $i++;
        }
        return new Individual(
            new KnapsackSolution($this->instance, $priceValue, $combination, -1),
            $weightValue
        );
    }

    public function getBestSurvival()
    {
        $maxQuality = 0;
        $bestIndividual = null;
        foreach ($this->individuals as $individual) {
            /** @var Individual $individual */
            $quality = $individual->getQuality($this->bestIndividualEverSeen);
            if ($maxQuality < $quality AND $individual->isValid()) {
                $maxQuality = $quality;
                $bestIndividual = $individual;
            }
        }
        return $bestIndividual;
    }

    /* *** SELECTION ***  */

    public function weakPerishStrongSurvive($maxSurvivals)
    {
        $newPopulation = [];
        while (true) {
            $arrIndex = array_rand($this->individuals, 4);

            if ($arrIndex === null) {
                break;
            }

            $arr = [];
            foreach ($arrIndex as $key) {
                $arr[] = $this->individuals[$key];
                unset ($this->individuals[$key]);
            }

            usort(
                $arr,
                function (Individual $a, Individual $b) {

                    // If is validity same, the quality differs.
                    if ($a->isValid() == $b->isValid()) {
                        if ($a->getQuality($this->bestIndividualEverSeen) == $b->getQuality(
                                $this->bestIndividualEverSeen
                            )
                        ) {
                            return 0;
                        }
                        return ($a->getQuality($this->bestIndividualEverSeen) < $b->getQuality(
                                $this->bestIndividualEverSeen
                            )) ? -1 : 1;
                    }

                    return ($a->isValid() < $b->isValid()) ? -1 : 1;
                }
            );

            $arr = array_reverse($arr);
            $newPopulation[] = $arr[0];

            if (count($newPopulation) >= $maxSurvivals) {
                break;
            }
        }

        $this->individuals = $newPopulation;
    }

    /* *** COMBINATION *** */

    public function multiply()
    {
        // Todo: how to select which two individuals should mate, best choice?
        $this->multipyPopulationOnlyInPairsTraditionally();
    }

    protected function getParents()
    {
        $poll = $this->individuals;
        $fathersCount = (int)(count($poll) / 2);
        $mothersCount = count($poll) - $fathersCount;

        $fathers = $this->getRandomSubArrayOfIndividuals($poll, $fathersCount);
        $mothers = $this->getRandomSubArrayOfIndividuals($poll, $mothersCount);

        return [$fathersCount, $fathers, $mothersCount, $mothers];
    }

    protected function getRandomSubArrayOfIndividuals(array &$poll, $count)
    {
        $arr = [];
        for ($i = 0; $i < $count; $i++) {
            $index = array_rand($poll);
            $arr[] = $poll[$index];
            unset ($poll[$index]);
        }
        return $arr;
    }

    protected function generateFirstGeneration()
    {
        for ($i = 0; $i < $this->sizeOfBasicPopulation; $i++) {
            $individual = $this->getRandomNewIndividual();
            $this->individuals[] = $individual;
            if (
                $this->bestIndividualEverSeen === null
                OR
                $this->bestIndividualEverSeen->getPriceValue() < $individual->getPriceValue()
            ) {
                $this->bestIndividualEverSeen = $individual;
            }
        }
    }


    protected function procreateNewChild(Individual $father, Individual $mother)
    {
        // == Multiply Father & Mother ==

        //   Todo: parametrize how we should select items to children
        $combination = $this->generateLongIntCombination();

        //   1 - take GEN from father
        $fathersPart = $father->getPartForReproduction($combination);
        //   0 - take GEN from mother

        $mothersPart = $mother->getPartForReproduction(~$combination); //  && pow(2, $clearShift) - 1

        $child = $fathersPart | $mothersPart;

        // == Mutation ==
        $mutation = $this->generateLongIntCombination();
//        $mutatedChild = $child;
        $mutatedChild = $child XOR $mutation;

        $childIndividual =  $this->createNewIndividual($mutatedChild);
        $this->testForBestIndividualEverSeen($childIndividual);
        return $childIndividual;
    }

    protected function multipyPopulationOnlyInPairsTraditionally()
    {
        // Incest possible
        // Monogamy, Monoadry - Children only in one exclusive pair.

        list ($fathersCount, $fathers, $mothersCount, $mothers) = $this->getParents();

        // If there is even count of individuals, one will not get chance to multiply
        for ($i = 0; $i < min($fathersCount, $mothersCount); $i++) {

            // Each pair can have more children
            for ($j = 0; $j < $this->growth; $j++) {
                $this->individuals[] = $this->procreateNewChild($fathers[$i], $mothers[$i]);
            }
        }
    }

    protected function generateLongIntCombination()
    {
        $combination = 0;
        $i = 0;
        while (true) {
            $combination |= rand(0, 1);
            $i++;

            if ($i >= $this->instance->getSize()) {
                break;
            }

            $combination <<= 1;
        }
        return $combination;
    }

    protected function testForBestIndividualEverSeen(Individual $individual)
    {
        if (
            $this->bestIndividualEverSeen === null
            OR
            $this->bestIndividualEverSeen->getPriceValue() < $individual->getPriceValue()
        ) {
            $this->bestIndividualEverSeen = $individual;
        }
    }

} 