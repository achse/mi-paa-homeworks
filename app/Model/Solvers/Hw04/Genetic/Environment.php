<?php

namespace Model\Solvers\Hw04\Genetic;


use Nette\Object;

class Environment extends Object {

    /** @var  int $capacity */
    protected $capacity;

    /** @var  Population */
    protected $population;

    function __construct($capacity)
    {
        $this->capacity = $capacity;
    }

    public function setPopulation($population)
    {
        $this->population = $population;
    }

    public function letTheTimeRun($numberOfGenerations) {
        for ($i = 0; $i < $numberOfGenerations; $i++) {
            $this->oneGeneration();
        }
    }

    /**
     * @return Individual
     */
    public function getBestSurvival()
    {
        return $this->population->getBestSurvival();
    }

    protected function oneGeneration()
    {
        $this->population->multiply();
        $this->population->weakPerishStrongSurvive($this->capacity);
    }

} 