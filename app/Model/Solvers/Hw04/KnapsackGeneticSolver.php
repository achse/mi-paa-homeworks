<?php

namespace Model\Solvers\Hw04;

use Model\Instance;
use Model\NoSolutionFoundException;
use Model\Solvers\AbstractKnapsackSolver;
use Model\Solvers\Hw04\Genetic\Environment;
use Model\Solvers\Hw04\Genetic\Population;

class KnapsackGeneticSolver extends AbstractKnapsackSolver
{

    protected function solveImplementation(Instance $instance)
    {
        // Konfiguration
        $numberOfGenerations = 40;
        $x = 256;
        $basicSizeOfPopulation = $x;
        $growth = 9;
        $environmentCapacity = $x;

        $environment = new Environment($environmentCapacity);
        $population = new Population($instance, $basicSizeOfPopulation, $growth);
        $environment->setPopulation($population);
        $environment->letTheTimeRun($numberOfGenerations);
        $bestIndividual = $environment->getBestSurvival();

        if ($bestIndividual === null) {
            throw new NoSolutionFoundException("Valid solution was NOT found in survived population.");
        }

        $solution =  $bestIndividual->getSolution();

        $this->bestSolutionPrice = $solution->getPriceValue();
        $this->bestSolutionCombination = $solution->getCombination();
        $this->lastRunTime = $solution->getTimeToGet();

    }

}