<?php

namespace Model\Utils;


use Symfony\Component\Console\Output\Output;

class Printer {

    protected static function _print($result, Output $output = null) {
        if ($output !== null) {
            $output->writeln($result);
        } else {
            echo ($result . "\n");
        }
    }

    public static function printArrayOfPairs($state, Output $output = null)
    {
        $arr = [];
        foreach ($state as $item) {
            $arr[] = "({$item[0]}, {$item[1]})";
        }
        self::_print("[" . join(' ', $arr). "]\n", $output);
    }

    public static function print2DArray($table, Output $output = null) {
        $arr = [];
        foreach ($table as $index => $row) {
            $row = array_map(function ($item) {
                    if ($item === null) {
                        return 'NIL';
                    } elseif ($item === PHP_INT_MAX) {
                        return '   ';
                    }
                    return str_pad($item, 3, ' ', STR_PAD_LEFT);
                }, $row);
            $arr[] = str_pad($index, 3, ' ', STR_PAD_LEFT) . ": [ " . join(' | ', $row) . " ]\n";
        }
        self::_print(" " .join(' ', $arr) . "\n", $output);
    }

} 