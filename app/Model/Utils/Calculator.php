<?php

namespace Model\Utils;


class Calculator {

    public static function calculateSumWeightAndPrice($state)
    {
        $sumWeight = 0;
        $sumPrice = 0;
        foreach ($state as $item) {
            $sumWeight += $item[0];
            $sumPrice += $item[1];
        }
        return [$sumWeight, $sumPrice];
    }

} 