<?php

namespace App\Model\Console;

use Model\Solvers\Hw02\KnapsackDynamicSolver;
use Symfony\Component\Console\Output\OutputInterface;

class KnapsackDynamicCommand extends AbstractKnapsackCommand
{

    public function createSolver(OutputInterface $output, array $settings)
    {
        return new KnapsackDynamicSolver($output, $settings);
    }

    public function getCommandName()
    {
        return 'knapsack:dynamic';
    }
}