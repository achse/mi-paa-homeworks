<?php

namespace App\Model\Console;

use Model\Solvers\Hw02\KnapsackBnBSolver;
use Symfony\Component\Console\Output\OutputInterface;

class KnapsackBnBCommand extends AbstractKnapsackCommand
{

    public function createSolver(OutputInterface $output, array $settings)
    {
        return new KnapsackBnBSolver($output, $settings);
    }

    public function getCommandName()
    {
        return 'knapsack:bnb';
    }
}