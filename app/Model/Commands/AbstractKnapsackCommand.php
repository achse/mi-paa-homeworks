<?php

namespace App\Model\Console;

use Model\DataLoader;
use Model\Instance;
use Model\Metric\Accuracy;
use Model\Metric\Time;
use Model\NoSolutionFoundException;
use Model\SolutionLoader;
use Model\Solvers\IKnapsackSolver;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractKnapsackCommand extends Command
{
    protected $wwwDir;

    protected $settings = [];

    function __construct($wwwDir, $name = null)
    {
        parent::__construct($name);
        $this->wwwDir = $wwwDir;
    }

    protected function configure()
    {
        $this->setName($this->getCommandName())->setDescription('')
            ->setDefinition([
                    new InputArgument('fileNumber', InputArgument::REQUIRED, 'File number.'),
                    new InputOption('path', '', InputOption::VALUE_OPTIONAL, 'File path.'),
                    new InputOption('time', 't', InputOption::VALUE_NONE, 'Print time dump.'),
                    new InputOption('progress', 'p', InputOption::VALUE_NONE, 'Shows progress Bar.'),
                    new InputOption('no-solution', '', InputOption::VALUE_NONE, 'Hides solution line'),
                    new InputOption('accuracy', '', InputOption::VALUE_NONE, 'Shows accuracy.'),
                ]);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fileNumber = $input->getArgument('fileNumber');
        $path = $input->getOption('path');

        $dataLoader = new DataLoader("{$this->wwwDir}", "{$path}/knap_{$fileNumber}.inst.dat");

        if ($input->getOption('accuracy')) {
            $accuracy = new Accuracy($this->wwwDir, $fileNumber, $dataLoader);
        }

        if ($input->getOption('time')) {
            $time = new Time();
        }

        $solver = $this->createSolver($output, $this->settings + [
            'time' => $input->getOption('time'),
            'progressBar' => $input->getOption('progress'),
            'verbose' => $input->getOption('verbose'),
        ]);

        $dataLoader->reset();
        while (true) {
            /** @var Instance $instance */
            $instance = $dataLoader->getNext();
            if ($instance === null) {
                break;
            }

            try {
                $solution = $solver->solve($instance);
            } catch (NoSolutionFoundException $ex) {
                $output->writeln("{$instance->getId()} NoSolutionFoundException: {$ex->getMessage()}");
                continue;
            }

            $pre = '';
            $post = '';

            if ($input->getOption('accuracy')) {
                $post .= "  " . $accuracy->add($instance, $solution);
            }

            if ($input->getOption('time')) {
                $timeToGet = $solution->getTimeToGet();
                $time->add($solution);
                $post .= "  " . $timeToGet;
            }

            if (!$input->getOption('no-solution')) {
                $output->writeln($solution->toString($pre, $post));
            }
        }

        if ($input->getOption('accuracy')) {
            $accuracy->report($output, $dataLoader->getSize());
        }

        if ($input->getOption('time')) {
            $time->report($output);
        }
    }

    /**
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @param array $settings
     * @return IKnapsackSolver
     */
    abstract public function createSolver(OutputInterface $output, array $settings);

    /**
     * @return string
     */
    abstract public function getCommandName();
}