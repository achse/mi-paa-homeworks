<?php

namespace App\Model\Console;

use Model\Solvers\Hw01\KnapsackOptimalSolver;
use Symfony\Component\Console\Output\OutputInterface;

class KnapsackOptimalCommand extends AbstractKnapsackCommand
{

    public function createSolver(OutputInterface $output, array $settings)
    {
        return new KnapsackOptimalSolver($output, $settings);
    }

    public function getCommandName()
    {
        return 'knapsack:optimal';
    }
}