<?php

namespace App\Model\Console;

use Model\Solvers\Hw04\KnapsackGeneticSolver;
use Symfony\Component\Console\Output\OutputInterface;

class KnapsackGeneticCommand extends AbstractKnapsackCommand
{

    public function createSolver(OutputInterface $output, array $settings)
    {
        return new KnapsackGeneticSolver($output, $settings);
    }

    public function getCommandName()
    {
        return 'knapsack:genetic';
    }
}