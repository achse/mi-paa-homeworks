<?php

namespace App\Model\Console;

use Model\Solvers\Hw01\KnapsackRatioHeuristicSolver;
use Symfony\Component\Console\Output\OutputInterface;

class KnapsackRatioHeuristicCommand extends AbstractKnapsackCommand
{

    public function createSolver(OutputInterface $output, array $settings)
    {
        return new KnapsackRatioHeuristicSolver($output, $settings);
    }

    public function getCommandName()
    {
        return 'knapsack:ratio';
    }
}