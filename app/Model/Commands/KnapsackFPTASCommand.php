<?php

namespace App\Model\Console;

use Model\Solvers\Hw02\KnapsackFPTASSolver;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class KnapsackFPTASCommand extends AbstractKnapsackCommand
{

    protected function configure()
    {
        parent::configure();
        $this->getDefinition()->addArgument(
            new InputArgument('shift', InputArgument::REQUIRED, 'How much has will be data shifted.')
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->settings['shift'] = $input->getArgument('shift');
        parent::execute($input, $output);
    }

    public function createSolver(OutputInterface $output, array $settings)
    {
        return new KnapsackFPTASSolver($output, $settings);
    }

    public function getCommandName()
    {
        return 'knapsack:fptas';
    }
}