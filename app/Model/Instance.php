<?php

namespace Model;

class Instance {

    /** @var  int */
    protected $id;

    /** @var  int */
    protected $size;

    /** @var  int */
    protected $capacity;

    /** @var  array  */
    protected $data;

    /**
     * @param string $rawData
     */
    function __construct($rawData = null)
    {
        if ($rawData) {
            // Todo: fix possible out of array index error
            $arr = array_map('trim', explode(' ', $rawData));
            $this->id = $arr[0];
            $this->size = $arr[1];
            $this->capacity = $arr[2];

            for ($i = 3; $i < count($arr); $i += 2) {
                $this->data[] = [$arr[$i], $arr[$i + 1]] ;
            }
        }
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return int
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * @param int $capacity
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
        $this->size = count($data);
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}