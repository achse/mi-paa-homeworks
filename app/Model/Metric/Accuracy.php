<?php

namespace Model\Metric;

use Kdyby\Console\InvalidArgumentException;
use Model\DataLoader;
use Model\Instance;
use Model\RelativeSolutionLoader;
use Model\SolutionLoader;
use Model\Solvers\KnapsackSolution;
use Nette\Object;
use Symfony\Component\Console\Output\OutputInterface;

class Accuracy extends Object {

    /** @var  SolutionLoader */
    protected $solutionLoader;

    /** @var double  */
    protected $errorRateSum;

    /** @var double  */
    protected $errorRateMin;

    /** @var double  */
    protected $errorRateMax;

    /** @var  int */
    protected $size;

    /** @var  string */
    protected $wwwDir;

    function __construct($wwDir, $fileNumber, DataLoader $dataLoader)
    {
        $this->wwwDir = $wwDir;
//        try {
            $this->solutionLoader = new SolutionLoader($this->wwwDir, "knap_{$fileNumber}.sol.dat");
//        } catch (InvalidArgumentException $ex) {
//            $this->solutionLoader = new RelativeSolutionLoader($dataLoader);
//        }
        $this->reset();
    }

    public function report(OutputInterface $output, $universeSize)
    {
        $totalErrorRate = ($this->errorRateSum / $universeSize) * 100;
        $minErrorRate = $this->errorRateMin * 100;
        $maxErrorRate = $this->errorRateMax * 100;
        $output->writeln("Error Rate | Average {$totalErrorRate}% | Max {$maxErrorRate}% | Min {$minErrorRate}%");
    }

    public function add(Instance $instance, KnapsackSolution $solution)
    {
        $referenceSolutionPrice = $this->solutionLoader->getSolutionPrice($instance->getId());
        $accuracy = $solution->getAccuracy($referenceSolutionPrice);
        $errorRate = (1 - $accuracy);

        $this->errorRateSum += $errorRate;
        $this->errorRateMax = max($this->errorRateMax, $errorRate);
        $this->errorRateMin = min($this->errorRateMin, $errorRate);

        return $accuracy;
    }

    protected function reset()
    {
        $this->errorRateSum = 0;
        $this->errorRateMin = 1;
        $this->errorRateMax = 0;
    }
}