<?php

namespace Model\Metric;

use Model\Solvers\KnapsackSolution;
use Nette\Object;
use Symfony\Component\Console\Output\OutputInterface;

class Time extends Object {

    /** @var double  */
    protected $timeSum;

    /** @var double  */
    protected $timeMin;

    /** @var double  */
    protected $timeMax;

    /** @var  int */
    protected $size;

    function __construct()
    {
        $this->reset();
    }

    public function report(OutputInterface $output)
    {
        $averageTime = $this->timeSum / $this->size;
        $output->writeln("Time elapsed | Average {$averageTime} | Max {$this->timeMax} | Min {$this->timeMin} | Sum {$this->timeSum}");
    }

    public function add(KnapsackSolution $solution)
    {
        $time = $solution->getTimeToGet();
        $this->timeSum += $time;
        $this->timeMax = max($this->timeMax, $time);

        if ($this->timeMin !== null) {
            $this->timeMin = min($this->timeMin, $time);
        } else {
            $this->timeMin = $time;
        }

        $this->size++;
    }

    protected function reset()
    {
        $this->timeSum = 0;
        $this->timeMax = 0;
        $this->timeMin = null;
    }
}