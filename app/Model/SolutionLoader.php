<?php

namespace Model;

use Kdyby\Console\InvalidArgumentException;
use Nette\Object;

class SolutionLoader extends Object
{

    const KEY_SIZE = 0;

    const KEY_SOLUTION = 1;

    const KEY_COMBINATION = 2;

    const SOURCE_KEY_ID = 0;

    const SOURCE_KEY_SIZE = 1;

    const SOURCE_KEY_SOLUTION = 2;

    const SOURCE_OFFSET_COMBINATION = 4;

    /** @var  string */
    protected $wwwDir;

    /** @var  string */
    protected $name;

    /** @var  array */
    protected $solutions;

    /** @var  int */
    protected $size;

    public function __construct($wwwDir, $name)
    {
        $this->wwwDir = $wwwDir;
        $this->name = $name;

        $path = "{$this->wwwDir}/../tests/data/sol/{$name}";
        if (!file_exists($path)) {
            throw new InvalidArgumentException("Solution file '{$path}' does NOT exists or is not accessible.");
        }

        // Todo: fix support for all line delimiters (\r\n, \n, \r)
        $rawDataArr = explode("\n", file_get_contents($path));

        foreach ($rawDataArr as $line) {
            $arr = explode(' ', $line);
            $this->solutions[$arr[self::SOURCE_KEY_ID]] = [
                $arr[self::SOURCE_KEY_SIZE],
                $arr[self::SOURCE_KEY_SOLUTION],
                array_slice($arr, self::SOURCE_OFFSET_COMBINATION)
            ];
        }
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getSolutionPrice($id)
    {
        return $this->solutions[$id][self::KEY_SOLUTION];
    }


}