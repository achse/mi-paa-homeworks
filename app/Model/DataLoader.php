<?php

namespace Model;

use Kdyby\Console\InvalidArgumentException;
use Kdyby\Console\InvalidStateException;
use Nette\Object;

class DataLoader extends Object {

    /** @var  string */
    protected $wwwDir;

    /** @var  string */
    protected $name;

    /** @var  array */
    protected $rawDataArr;

    /** @var  int */
    protected $index = 0;

    /** @var  int */
    protected $size;

    /** @var  bool */
    protected $counted = false;

    public function __construct($wwwDir, $name)
    {
        $this->wwwDir = $wwwDir;
        $this->name = $name;

        $path = "{$this->wwwDir}/../tests/data/inst/{$name}";
        if (!file_exists($path)) {
            throw new InvalidArgumentException("Source file '{$path}' does NOT exists or is not accessible.");
        }

        // Todo: fix support for all line delimiters (\r\n, \n, \r)
        $this->rawDataArr = explode("\n", file_get_contents($path));
        $this->size = count($this->rawDataArr);
    }

    public function reset() {
        $this->index = 0;
    }

    /**
     * @return Instance
     */
    public function getNext() {
        // EOF
        if ($this->index >= $this->size) {
            $this->counted = true;
            return null;
        }

        // Skip empty lines
        if (trim($this->rawDataArr[$this->index]) === '') {
            $this->index++;
            $this->size--;
            return $this->getNext();
        }

        $instance = new Instance($this->rawDataArr[$this->index]);
        $this->index++;
        return $instance;
    }

    public function getSize()
    {
        if (!$this->counted) {
            throw new InvalidStateException("Input data was not counted yet.");
        }
        return $this->size;
    }



}