#!/bin/sh

sudo mkdir temp log
sudo chmod 0777 temp log tests/data
composer install

sudo rm -fr log/*
sudo rm -fr temp/*
