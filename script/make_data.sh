#!/bin/bash

array=( 4 10 15 20 22 25 27 30 32 35 37 40 )
for i in "${array[@]}"
do
    echo $i
    echo php www/index.php knapsack:$2 $i $3 $4 $5 $6 $7 $9 $9 > tests/data/$1/$2$i.log
    php www/index.php knapsack:$2 $i $3 $4 $5 $6 $7 $8 $9 >> tests/data/$1/$2$i.log
done
