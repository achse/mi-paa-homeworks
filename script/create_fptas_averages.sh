#!/bin/bash

array=( 4 10 15 20 22 25 27 30 32 35 37 40 )
for i in "${array[@]}"
do
    cat tests/data/hw2-fptas_$1/fptas$i.log | gawk -F "   " '{print $2}' | awk '{ total += $1; count++ } END { print total/count }'
done
