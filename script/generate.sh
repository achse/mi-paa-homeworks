#!/bin/bash

# This script is just a scratch and is parametrized by simply
# direct "in place" edit in code.

# array=( 0.05 0.1 0.2 0.5 1 2 3 4 5 6 7 8 9 10 100 )
#array=( 100 200 300 400 500 600 700 800 900 1000 )



array=( 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1)

for i in "${array[@]}"
do
    M=m
    echo Generating -${M} ${i}
    ./tests/knapgen/knapsack -n 20 -N 200 -m $i -W 100 -C 100 -k 2 -d 0 2> /dev/null > tests/3/knap_20_mod_${M}_$i.inst.dat
    echo Calculatin solution -${M} ${i}
    php www/index.php knapsack:dynamic 20_mod_${M}_$i --path ../../3 > tests/data/sol/knap_20_mod_${M}_$i.sol.dat

#    M=W
#    echo Generating -${M} ${i}
#    ./tests/knapgen/knapsack -n 20 -N 200 -m 0.5 -W $i -C 100 -k 2 -d 0 2> /dev/null > tests/3/knap_20_m_0.5_mod_${M}_$i.inst.dat
#    echo Calculatin solution -${M} ${i}
#    php www/index.php knapsack:dynamic 20_m_0.5_mod_${M}_$i --path ../../3 > tests/data/sol/knap_20_m_0.5_mod_${M}_$i.sol.dat
done


array=( 0.05 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 2 3)

for i in "${array[@]}"
do
    M=k
    echo Generating -${M} ${i}
    ./tests/knapgen/knapsack -n 20 -N 200 -m 0.5 -W 100 -C 100 -k $i -d -1 2> /dev/null > tests/3/knap_20_m_0.5_mod_${M}_$i.inst.dat
    echo Calculatin solution -${M} ${i}
    php www/index.php knapsack:dynamic 20_m_0.5_mod_${M}_$i --path ../../3 > tests/data/sol/knap_20_m_0.5_mod_${M}_$i.sol.dat
done
