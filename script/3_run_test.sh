#!/bin/bash

# array=( 200 400 600 800 1000 1000000 )
#array=( 100 200 300 400 500 600 700 800 900 1000 )
# array=( 0.05 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 2 3 4)
# array=( 0.05 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 2 3 4)
#array=( 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1)
array=( 0.05 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1 2 3)

M=k
SIZE=20

for i in "${array[@]}"
do
    echo -n -e $i "\t"
    php www/index.php knapsack:$1 $2 ${SIZE}_m_0.5_mod_${M}_$i --path ../../3 -t | tail -n 2 | head -n 1 | cut -d' ' -f5
done